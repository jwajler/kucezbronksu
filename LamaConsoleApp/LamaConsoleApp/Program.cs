﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace LamaConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type 1 if you want to see documents for category and probability of matching");
            Console.WriteLine("Type 2 if you want to see document with matching");
            string option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    {
                        Console.WriteLine("Enter category: ");
                        string _category = Console.ReadLine();
                        Console.WriteLine("Specify the probability of matching the document to the category (0 - 1): ");
                        string _vector = Console.ReadLine();

                        GetHashStringFromCategoryAndVecotr(Convert.ToInt32(_category),
                            Convert.ToDouble(_vector, CultureInfo.InvariantCulture));

                        break;
                    }

                case "2":
                    {
                        //sample: 00a175d0d29b006f48607a4967923aa67df20ee8
                        Console.WriteLine("Enter hashstring of document: ");
                        string _hashstring = Console.ReadLine();
                        ShowDataForHashstring(_hashstring);

                        break;
                    }
            }
        }


        public static List<string> GetHashStringFromFolders() //list of all hashstrings in subdirectories.
        {
            var dataFilesPath = Directory.GetDirectories(@"C:\Users\Asia\Desktop\EXATEL_DATA"); //u need path to this one.
            var dataFilePathString = dataFilesPath.ToString();
            var hashList = new List<string>();

            foreach (var file in dataFilesPath)
            {
                var filePathArray = Directory.GetFiles(file);
                foreach (var hashName in filePathArray)
                {
                    string fileName = Path.GetFileNameWithoutExtension(hashName);
                    hashList.Add(fileName);

                }
            }
            return hashList;
        }

        public static void GetHashStringFromCategoryAndVecotr(int category, double vector)
        {
            var sourceData = Path.Combine(Environment.CurrentDirectory, "Data_From_Patrick.txt");
            var hashStringsWithParams = File.ReadAllLines(sourceData);

            var modelWithNumbers = new Dictionary<string, (int, double)>();
            foreach (var m in hashStringsWithParams)
            {
                var hashstringAndParams = m.Split("[");
                var paramsNotClear = hashstringAndParams[1].Split(",");
                var onlyHash = hashstringAndParams[0];

                var onlyStringParams = new List<string>();
                foreach (var p in paramsNotClear)
                {
                    var clear = p.Replace("(", "").Replace(")", "").Replace("]", "");
                    onlyStringParams.Add(clear);
                }

                int stringParamsLength = onlyStringParams.Count;
                var floor = "_";

                while (stringParamsLength > 0)
                {
                    modelWithNumbers.Add(onlyHash, (Convert.ToInt32(onlyStringParams.First()),
                    Convert.ToDouble(onlyStringParams.Skip(1).First(), CultureInfo.InvariantCulture)));

                    onlyStringParams.RemoveRange(0, 2);
                    stringParamsLength -= 2;
                    onlyHash += floor;
                }
            }

            var filteredHash = modelWithNumbers.Where(v => v.Value.Item1 == category && v.Value.Item2 >= vector);
            var resultHashes = new List<string>();
            if (filteredHash.Any())
            {
                foreach (var f in filteredHash)
                {
                    //resultHashes.Add(f.Key.Replace("_", ""));
                    Console.WriteLine(f.Key.Replace("_", ""));
                }
            }
            else
                Console.WriteLine("Sorry, no matching results.");

            Console.ReadLine();
        }

        public static void ShowDataForHashstring(string hashstring)
        {
            var sourceData = Path.Combine(Environment.CurrentDirectory, "Data_From_Patrick.txt");
            var hashStringsWithParams = File.ReadAllLines(sourceData);

            var result = hashStringsWithParams.FirstOrDefault(h => h.Contains(hashstring));
            Console.WriteLine("Your result: ");
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
