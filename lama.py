
import gensim
import pickle 
import os
from gensim.test.utils import common_texts 
from gensim.corpora.dictionary import Dictionary 
import os.path		 
from os import listdir
from os.path import isfile, join
import random
import re

#data_path = r"C:\Users\plasota\plasota\hackaton\kucezbronksu\ZealousLama\ZealousLama.Service\Data\issues_train.tsv"
data_path = r"H:\00a"

onlyfiles = [f for f in listdir(data_path) if isfile(join(data_path, f))]


for file in onlyfiles:
	file_path = f"{data_path}\{file}"

	f = open(file_path, "r")
	lines = f.readlines()

	tablica = []

	print("!!!!!!!!!!q")
	string_lines = ''.join(lines)
	#input = string_lines.split()
		
	space_separated_lines = string_lines.replace('\n', ' ').replace('\r', '')
	#for index, l in enumerate(string_lines):
		#tablica.append(l.split())
	splitted_data = space_separated_lines.split()
	only_string = []
	for splitted in splitted_data:
		no_number = re.sub("\d", "", splitted)
		only_string.append(no_number)

	text_data = [only_string]
	#text_data = [space_separated_lines.split()]
	
	common_dictionary = Dictionary(text_data) 
	#common_dictionary = text_data
	print(common_dictionary)
	common_corpus = [common_dictionary.doc2bow(text) for text in text_data]  
	#common_corpus = common_dictionary.doc2bow(common_dictionary)   

	ldamodel = gensim.models.ldamodel.LdaModel(common_corpus, num_topics=20, id2word = common_dictionary)    

	ldamodel.save('model5.gensim')
	topics = ldamodel.print_topics(num_words=4) 

	for topic in topics:
		print(topic)  

	other_texts = [ ['computer', 'time', 'graph'], ['survey', 'response', 'eps'], ['human', 'system', 'computer'] ]  

	other_corpus = [common_dictionary.doc2bow(text) for text in other_texts]
	unseen_doc = other_corpus[0]
	vector = ldamodel[unseen_doc]  
	print(file_path)
	print(unseen_doc)
	print()
	print(vector)
	